import { Injectable } from '@angular/core';
import * as alerity from 'alertifyjs';

@Injectable({
  providedIn: 'root'
})
export class AlertifyService {

constructor() {}

  confirm(message:string, oKCallback: ()=> any){
    alerity.confirm(message,(e:any)=>{
      if(e){
        oKCallback();
      }
      else
      {}

    });
  }

  success(message:string){
    alerity.success(message);
  }

  error(message:string){
    alerity.error(message);
  }

  warning(message:string){
    alerity.warning(message);
  }

  message(message:string){
    alerity.message(message);
  }


}
